package com.HRIS2.organization.test;

import com.HRIS2.Order;
import com.HRIS2.OrderedRunner;
import com.jayway.restassured.response.ValidatableResponse;
import com.service.GenerateDataService;
import com.service.RestService;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

@RunWith(OrderedRunner.class)
public class OrganizationTest {
    RestService rs = new RestService("/organizations/", 1.0);
    GenerateDataService df = new GenerateDataService();
    static String currentId;

    @Test
    @Order(order = 1)
    public void create() {
        Map data = df.orgainzationForm("create");
        ValidatableResponse response = rs.create(data);
        this.currentId = response.extract().path("data.id");
        response.statusCode(200);
        response.body("status", equalTo(true));
        response.body("data.nameTh", equalTo(data.get("nameTh")));
//        System.out.println(response.extract().response().getBody().asString());
    }

    @Test
    @Order(order = 2)
    public void listAll() {
        ValidatableResponse response = rs.listAll();
        response.body("data.id", hasItems(this.currentId));
    }

    @Test
    @Order(order = 3)
    public void getById() {
        Map data = df.orgainzationForm("create");
        ValidatableResponse response = rs.getById(this.currentId);
        response.body("id", equalTo(this.currentId));
        response.body("nameTh", equalTo(data.get("nameTh")));
    }

    @Test
    @Order(order = 4)
    public void update() {
        Map data = df.orgainzationForm("update");
        ValidatableResponse response = rs.update(this.currentId, data);
        response.statusCode(200);
        response.body("data.id", equalTo(this.currentId));
        response.body("data.nameTh", equalTo(data.get("nameTh")));
    }

    @Test
    @Order(order = 5)
    public void delete() {
        ValidatableResponse response = rs.delete(this.currentId);
        response.statusCode(200);
        response.body("status", equalTo(true));
    }
}
