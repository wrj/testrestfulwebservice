package com.HRIS2.department.test;

import com.HRIS2.Order;
import com.HRIS2.OrderedRunner;
import com.jayway.restassured.response.ValidatableResponse;
import com.service.GenerateDataService;
import com.service.RestService;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.equalTo;

@RunWith(OrderedRunner.class)
public class DepartmentTest {

    RestService cs = new RestService("/organizations/", 1.0);
    RestService rs = new RestService("/departments/", 1.0);
    GenerateDataService df = new GenerateDataService();
    static String currentId;
    static String parentId;
    static String orgainzationId;

    @Test
    @Order(order = 1)
    public void create() {
        ValidatableResponse cr = cs.create(df.orgainzationForm("create"));
        this.orgainzationId = cr.extract().path("data.id");
        Map data = df.departmentForm("create", this.orgainzationId,"");
        ValidatableResponse response = rs.create(data);
        this.currentId = response.extract().path("data.id");
        response.statusCode(200);
        response.body("data.departmentNameTh", equalTo(data.get("departmentNameTh")));
    }

    @Test
    @Order(order = 2)
    public void listAll() {
        ValidatableResponse response = rs.listAll();
        response.body("data.id", hasItems(this.currentId));
    }

    @Test
    @Order(order = 3)
    public void getById() {
        ValidatableResponse response = rs.getById(this.currentId);
        response.body("id", equalTo(this.currentId));
    }

    @Test
    @Order(order = 4)
    public void update() {
        Map data = df.departmentForm("update", this.orgainzationId,"");
        ValidatableResponse response = rs.update(this.currentId, data);
        response.statusCode(200);
        response.body("data.id", equalTo(this.currentId));
        response.body("data.departmentNameTh", equalTo(data.get("departmentNameTh")));
    }

    @Test
    @Order(order = 5)
    public void createChildren() {
        this.parentId = this.currentId;
        Map data = df.departmentForm("create", this.orgainzationId,this.parentId);
        ValidatableResponse response = rs.create(data);
        this.currentId = response.extract().path("data.id");
        response.statusCode(200);
        response.body("data.departmentNameTh", equalTo(data.get("departmentNameTh")));
    }

    @Test
    @Order(order = 6)
    public void getChildrenById() {
        ValidatableResponse response = rs.getById(this.currentId);
        response.body("divisionId", equalTo(this.parentId));
    }

    @Test
    @Order(order = 7)
    public void deleteChildren() {
        ValidatableResponse response = rs.delete(this.currentId);
        response.statusCode(200);
        response.body("status", equalTo(true));
    }

    @Test
    @Order(order = 8)
    public void delete() {
        ValidatableResponse response = rs.delete(this.parentId);
        ValidatableResponse cr = cs.delete(this.orgainzationId);
        response.statusCode(200);
        response.body("status", equalTo(true));
    }

}
