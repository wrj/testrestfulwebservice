package com.HRIS2;


import com.HRIS2.bank.test.BankTest;
import com.HRIS2.certificate.test.CertificateTest;
import com.HRIS2.department.test.DepartmentTest;
import com.HRIS2.educationLevel.test.EducationLevelTest;
import com.HRIS2.organization.test.OrganizationTest;
import com.HRIS2.position.test.PositionTest;
import com.HRIS2.trainingtype.test.TrainingTypeTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        OrganizationTest.class,
        DepartmentTest.class,
        BankTest.class,
        EducationLevelTest.class,
        CertificateTest.class,
        TrainingTypeTest.class
})
public class JunitTestSuite {
}