package com.service;

import com.jayway.restassured.response.ValidatableResponse;
import java.util.Map;
import static com.jayway.restassured.RestAssured.*;


public class RestService {

    String URL = "";
    Double restVersion = 1.0;

    public RestService(String url, Double restVersion) {
        baseURI = "http://192.168.1.14";
        port = 8080;
        this.URL = url;
        this.restVersion = restVersion;
    }

    public RestService(String url) {
        baseURI = "http://192.168.1.14";
        port = 8080;
        this.URL = url;
    }

    public ValidatableResponse listAll() {
        ValidatableResponse response =
                given().
                        contentType("application/json").
                        header("Accept-Version", restVersion).
                        when().
                        get(this.URL + "list").
                        then();
        return response;
    }

    public ValidatableResponse getById(String id) {
        ValidatableResponse response =
                given().
                        contentType("application/json").
                        header("Accept-Version", restVersion).
                        when().
                        get(this.URL + id).
                        then();
        return response;
    }

    public ValidatableResponse delete(String id) {
        ValidatableResponse response =
                given().
                        contentType("application/json").
                        header("Accept-Version", restVersion).
                        when().
                        delete(this.URL + id).
                        then();
        return response;
    }

    public ValidatableResponse create(Map map) {
        ValidatableResponse response =
                given().
                        contentType("application/json; charset=UTF-8").
                        header("Accept-Version", restVersion).
                        body(map).
                        when().
                        post(this.URL).
                        then();
        return response;
    }

    public ValidatableResponse update(String id, Map map) {
        ValidatableResponse response =
                given().
                        contentType("application/json; charset=UTF-8").
                        header("Accept-Version", restVersion).
                        body(map).
                        when().
                        put(this.URL + id).
                        then();
        return response;
    }
}
