package com.service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by piyawat on 6/14/2016 AD.
 */
public class GenerateDataService {

    public GenerateDataService() {
    }

    public Map orgainzationForm(String action) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("nameTh", "ไมตี้");
        jsonAsMap.put("nameEng", "Mighty");
        jsonAsMap.put("logo", "");
        jsonAsMap.put("parentOrganization", "");
        jsonAsMap.put("organizationType", 0);
        jsonAsMap.put("branchNo", 0);
        jsonAsMap.put("noTaxpayers", "11223344");
        jsonAsMap.put("founderDate", "01/01/1990");
        jsonAsMap.put("addressName", "Mighty.co.th");
        jsonAsMap.put("addressNo", "59/481");
        jsonAsMap.put("addressAlley", "");
        jsonAsMap.put("addressLane", "พระราม 2/52");
        jsonAsMap.put("addressRoad", "พระราม 2");
        jsonAsMap.put("addressSubDistrict", "LSDIS_00149");
        jsonAsMap.put("addressDistrict", "LDIS_00021");
        jsonAsMap.put("addressProvince", "LPRO_00001");
        jsonAsMap.put("addressPostalCode", 10150);
        jsonAsMap.put("addressMap", "");
        jsonAsMap.put("telephone1", "028957467");
        jsonAsMap.put("telephone2", "");
        jsonAsMap.put("telephone3", "");
        jsonAsMap.put("fax", "028957469");
        jsonAsMap.put("website", "www.mighty.com");
        jsonAsMap.put("email", "mighty@gmail.com");
        jsonAsMap.put("remark", "");
        if (action == "update") {
            jsonAsMap.put("nameEng", "Mightymug");
            jsonAsMap.put("nameTh", "ไมตี้มัค");
        }
        return jsonAsMap;
    }

    public Map departmentForm(String action, String orgainzationId,String parentId) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        if (parentId == "")
        {
            jsonAsMap.put("departmentNameTh", "สำนักงานฝ่าย");
            jsonAsMap.put("departmentNameEng", "Division");
            jsonAsMap.put("description", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("departmentType", 0);
            jsonAsMap.put("orgainzationId", orgainzationId);
            if (action == "update") {
                jsonAsMap.put("departmentNameTh", "สำนักงานฝ่ายโรงพยาบาล");
            }
        }else{
            jsonAsMap.put("departmentNameTh", "หน่วยงาน");
            jsonAsMap.put("departmentNameEng", "Work");
            jsonAsMap.put("description", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("departmentType", 1);
            jsonAsMap.put("divisionId", parentId);
            jsonAsMap.put("orgainzationId", orgainzationId);
            if (action == "update") {
                jsonAsMap.put("departmentNameTh", "หน่วยงานโรงพยาบาล");
            }
        }
        return jsonAsMap;
    }

    public Map positionForm(String action, String departmentId,String parentId) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        if(parentId == "")
        {
            jsonAsMap.put("positionNameTh", "ผู้จัดการ");
            jsonAsMap.put("positionNameEng", "Manager");
            jsonAsMap.put("description", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("parentId", "");
            jsonAsMap.put("certificateId", "LCER_00003");
            jsonAsMap.put("majorId", "LMAJ_00002");
            jsonAsMap.put("fromAge", 25);
            jsonAsMap.put("toAge", 40);
            jsonAsMap.put("gender", "male");
            jsonAsMap.put("detail", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("departmentId", departmentId);
            if (action == "update") {
                jsonAsMap.put("positionNameTh", "ผู้จัดการทั่วไป");
                jsonAsMap.put("positionNameEng", "General Manager");
            }
        }else{
            jsonAsMap.put("positionNameTh", "เลขา");
            jsonAsMap.put("positionNameEng", "Secretary");
            jsonAsMap.put("description", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("parentId", parentId);
            jsonAsMap.put("certificateId", "LCER_00003");
            jsonAsMap.put("majorId", "LMAJ_00002");
            jsonAsMap.put("fromAge", 25);
            jsonAsMap.put("toAge", 30);
            jsonAsMap.put("gender", "female");
            jsonAsMap.put("detail", "Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16");
            jsonAsMap.put("departmentId", departmentId);
            if (action == "update") {
                jsonAsMap.put("positionNameTh", "เลขาผู้จัดการทั่วไป");
                jsonAsMap.put("positionNameEng", "Secretary General Manager");
            }
        }
        return jsonAsMap;
    }

    public Map trainingTypeForm(String action, String orgainzationId) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("orgainzationId", orgainzationId);
        jsonAsMap.put("title", "Network Security Training");
        if (action == "update") {
            jsonAsMap.put("title", "Cloud computing");
        }
        return jsonAsMap;
    }

    public Map bankForm(String action) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("name", "Land House");
        if (action == "update") {
            jsonAsMap.put("name", "Land and House");
        }
        return jsonAsMap;
    }

    public Map educationLevelForm(String action) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("educationLevelNameTh", "ประถม");
        jsonAsMap.put("educationLevelNameEng", "school");
        jsonAsMap.put("priority", 8);
        if (action == "update") {
            jsonAsMap.put("educationLevelNameTh", "ประถมต้น");
            jsonAsMap.put("educationLevelNameEng", "first school");
        }
        return jsonAsMap;
    }

    public Map certificateForm(String action, String orgainzationId,String educationLevelId) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("orgainzationId", orgainzationId);
        jsonAsMap.put("certificateNameTh", "ซีซีเอ็น");
        jsonAsMap.put("certificateNameEng", "CCN");
        jsonAsMap.put("educationLevelId", educationLevelId);
        if (action == "update") {
            jsonAsMap.put("certificateNameTh", "ซีซีเอ็นเอ");
            jsonAsMap.put("certificateNameEng", "CCNA");
        }
        return jsonAsMap;
    }

    public Map majorForm(String action, String orgainzationId) {
        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("orgainzation", orgainzationId);
        jsonAsMap.put("majorNameTh", "ไฟฟ้า");
        jsonAsMap.put("majorNameEng", "Electric");
        if (action == "update") {
            jsonAsMap.put("majorNameTh", "ไฟฟ้ากำลัง");
        }
        return jsonAsMap;
    }
}
